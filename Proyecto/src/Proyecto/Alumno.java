package Proyecto;

public class Alumno {
	private String nia;
	private String nombre;
	private String apellidos;
	private Fecha fecha;
	
	public Alumno(String nia, String nombre, String apellidos, Fecha fecha) {
		this.apellidos= apellidos;
		this.fecha=fecha;
		this.nombre= nombre;
		this.nia=nia;
	}

	public String getNia() {
		return nia;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public Fecha getFecha() {
		return fecha;
	}
	
}
